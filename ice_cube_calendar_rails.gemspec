$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ice_cube_calendar_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ice_cube_calendar_rails"
  s.version     = IceCubeCalendarRails::VERSION
  s.authors     = ["Tamiswanashe Chipika"]
  s.email       = ["tchipika@gmail.com"]
  s.homepage    = "http://quickreportsystems.com/"
  s.summary     = "internal QRS schedualling library"
  s.description = "Wraps Icecube and provides a clean easy to use interface for schedualing and reoccuring event managment"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.required_ruby_version = '>= 2.1.5'
  s.add_dependency "rails", ">= 4.2"
  s.add_dependency "ice_cube", "~> 0.16.0"

  s.add_development_dependency "pg"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "factory_girl_rails"
  s.add_development_dependency "guard-rspec"
end
