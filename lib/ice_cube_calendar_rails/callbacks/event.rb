module IceCubeCalendarRails
  module Callbacks
    module Event

      protected
        def set_schedule_end
          self.schedule.rrules.each do |rule|
            if self.num_occurrences.nil?
              rule.until(self.until_at)
            elsif self.num_occurrences.present?
              rule.count(num_occurrences)
            end
          end
        end

        def remove_instances_after_until_at
          instances.where('start_at > ?', self.until_at).destroy_all
        end

        def remove_instances_after_last_occurrence
          instances.order(start_at: :asc).offset(self.num_occurrences).destroy_all
        end

        def create_single_event_instance
          create_event_instance(self.start_at, self.end_at)
        end
    end
  end
end