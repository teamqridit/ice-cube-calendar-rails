module IceCubeCalendarRails
  module List
    module Event

      module Finders
        def earlier(start_time, original_event_id: nil)
          if original_event_id.nil?
            self.where("start_at < ?", start_time)
          else
            self.where("start_at < ? AND original_event_id = ?", start_time, original_event_id)
          end
        end

        def later(start_time, event_id, original_event_id: nil)
          if original_event_id.nil?
            self.where("start_at > ?", start_time)
          else
            self.where("start_at > ? AND original_event_id = ?", start_time, original_event_id)
          end
        end
      end

      module ClassMethods
        include ::IceCubeCalendarRails::List::Event::Finders

        def all_instances(start_at:, end_at:)
          # we're going to obtain a pessimistic lock the Event objects we retrieve here since some of the expansion is now
          # going to be taking place on a background job in the parent app
          # We're also going to restrict this expensive operation to just Events that can possibly fit inside the window we're interested in
          lock.where(single_instance: false).where('start_at >= :start AND start_at < :end', start: start_at, end: end_at)
            .find_each do |event|

              usable_start_at = start_at > event.start_at ? event.start_at : start_at
            event.expand(start_at: usable_start_at, end_at: end_at)
          end

          instance_klass.find_by_date_range(start_at, end_at)#.where(deleted: false)
        end
      end

      module InstanceMethods

        def create_recurring_event_instance(occurrence)
          create_event_instance(occurrence, occurrence + duration)
        end

        def expand(start_at:, end_at:)
          # create a cache key so we can cache the relatively expensive count queries

          # cache_buster_instance = self.instances.where('start_at >= :start_at AND end_at > :start_at', start_at: start_at).order(updated_at: :desc).first

          # # just use the last created instance in this case
          # cache_buster_instance = self.instances.last if cache_buster_instance.nil?
          # cache_buster = cache_buster_instance.present? ? cache_buster_instance.updated_at.to_i : self.updated_at.to_i

          instances_count_cache_key = "#{cache_key}-event_instances-#{start_at.to_i}-#{end_at.to_i}"

          self.reload
          occurrences = self.schedule.occurrences_between(start_at, end_at)
          occurrences_to_expand = []

          puts "----- occurrences is #{occurrences.inspect} --------------------"
          # puts "num occurrences is #{occurrences.count.inspect} for #{start_at} - #{end_at}"

          # expanded_instance_count = Rails.cache.fetch(instances_count_cache_key) do

            result = self.instances.find_by_date_range_for_expansion_count(start_at, end_at).count

            result
          # end
          expanded_instance_count = result

          if expanded_instance_count == 0
            # make sure there really aren't any instances by running a query...
            expanded_instance_count_query_result = self.instances.find_by_date_range_for_expansion_count(start_at, end_at).count

            if expanded_instance_count_query_result > 0
              # cache has actually expired...

              Rails.cache.write(instances_count_cache_key, expanded_instance_count_query_result)

              expanded_instance_count = expanded_instance_count_query_result
            end
          end
          
          # expanded_instance_count = self.instances.find_by_date_range(start_at, end_at).count

          if occurrences.count > expanded_instance_count
            # there are some occurrences that haven't been expanded yet
            unexpanded_instance_count = occurrences.count - expanded_instance_count

            occurrences_to_expand = occurrences[expanded_instance_count, unexpanded_instance_count]
          end

          expanded_events = []

          puts "----- occurrences_to_expand is #{occurrences_to_expand.inspect} --------------------"
          
          occurrences_to_expand.each do |occurrence|
            expanded_events << self.create_recurring_event_instance(occurrence)
          end

          if expanded_events.size > 0
            Rails.cache.delete(instances_count_cache_key)
          end

          expanded_events
        end

        def update_start_end_times
          occurrences_to_update = self.schedule.first(self.instances.count)

          self.instances.order(id: :asc).each_with_index do |instance, i|

            if (i + 1) > occurrences_to_update.count
              instance.destroy
            else
              instance.update!(suppress_update_future_occurrences: true, start_at: occurrences_to_update[i], end_at: occurrences_to_update[i] + instance.duration)
            end
          end
        end
      end
    end
  end
end