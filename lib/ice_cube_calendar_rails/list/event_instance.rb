module IceCubeCalendarRails
  module List
    module EventInstance

      module Finders
        def find_by_date_range(start_at, end_at)
          where("start_at >= ? AND end_at <= ?", start_at, end_at)
          # where("tsrange(start_at, end_at,'[]') && tsrange(:start_at,:end_at,'[]')", { start_at: start_at, end_at: end_at })
        end

        def find_by_date_range_for_expansion_count(start_at, end_at)
          where("(start_at >= :start AND end_at <= :end) OR (original_start_at >= :start AND original_end_at <= :end)", start: start_at, end: end_at)
          # where("tsrange(start_at, end_at,'[]') && tsrange(:start_at,:end_at,'[]')", { start_at: start_at, end_at: end_at })
        end

        def earlier_in_series(start_time, event_id, original_event_ids: [])
          self.where("start_at < ? AND (event_id = ? OR event_id IN (?) )", start_time, event_id, original_event_ids)
        end

        def later_in_series(start_time, event_id, original_event_ids: [])
          self.where("start_at > ? AND (event_id = ? OR event_id IN (?) )", start_time, event_id, original_event_ids)
        end
      end

      module ClassMethods
        include ::IceCubeCalendarRails::List::EventInstance::Finders
      end

      module InstanceMethods
      end

      
    end
  end
end