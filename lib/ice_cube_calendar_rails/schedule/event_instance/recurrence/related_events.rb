module IceCubeCalendarRails
  module Schedule

    module EventInstance

      module Recurrence

        module RelatedEvents

          # return all the events that this event instance is related to
          def related_event_ids
            related_ids(self.event)
            # if self.event.original_event_id.nil?
            #   original_event_ids = [self.event.id]
            #   original_event_ids = original_event_ids + self.event.class.where(original_event_id: self.event.id).pluck(:id)
            # else
            #   original_event_ids = self.event.class.where(original_event_id: self.event.original_event_id).pluck(:id)
            # end
          end

          def next_related_event
            if self.event.original_event_id.present?
              next_event = self.event.class.where.not(id: self.event.id).where(original_event_id: self.event.original_event_id).order(start_at: :asc).first
            else
              next_event = self.event.class.where(original_event_id: self.event.id).order(start_at: :asc).first
            end

            next_event
          end

          def related_events
            if self.saved_changes["start_at"].present?
              usable_start_at = self.saved_changes["start_at"].first
            else
              usable_start_at = self.start_at
            end

            if self.event.original_event_id.nil?
              related_events = self.event.class.where('start_at >= ?', usable_start_at)
                .where(original_event_id: self.event.id)
            else
              related_events = self.event.class.where.not(id: self.event.id).where('start_at >= ?', usable_start_at)
                .where(original_event_id: self.event.original_event_id)
            end

            related_events
          end
        end
      end
    end
  end
end