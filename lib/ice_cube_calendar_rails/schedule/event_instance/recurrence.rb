module IceCubeCalendarRails
  module Schedule

    module EventInstance

      module Recurrence

        include RelatedEvents

        def until_at
          @until_at ||= self.event.try(:until_at)
        end

        def until_at=(value)
          attribute_will_change!('until_at') if until_at != value
          @until_at = value
        end

        def num_occurrences
          @num_occurrences ||= self.event.try(:num_occurrences)
        end

        def num_occurrences=(value)
          attribute_will_change!('num_occurrences') if num_occurrences != value
          @num_occurrences = value
        end

        protected

          def delete_instances
            is_first = self.class.earlier_in_series(self.start_at, self.event.id).count == 0

            if is_first
              # just delete the event, nice and simple :)
              self.event.destroy
            else
              end_recurrence_at_and_delete
            end

            # destroy all future occurrences across all related events
            ids = related_event_ids

            self.class.later_in_series(self.start_at, self.event.id, original_event_ids: ids).destroy_all
            self.event.class.where(id: ids - [self.event.id]).destroy_all
          end

          def update_future_occurrences
            if this_and_all_following

              # find all the events that are related to this instance
              ids = related_event_ids

              self.class.later_in_series(self.start_at, self.event.id, original_event_ids: ids)
                .each do |instance|
                  instance_attributes = { suppress_update_future_occurrences: true }

                  event_attributes.each do |attribute|
                    instance_attributes[attribute] = self.send attribute
                  end
                  unless self.event.event_associations.nil?

                    self.event.event_associations.each do |association|
                      
                      # start by removing any existing associations on the future occurrences and then making them match the current instance
                      case instance.class.reflect_on_association(association[:key]).macro
                        when :has_many
                          instance.send "#{association[:key].to_s}=".to_sym, []
                        when :has_one
                          instance.send "#{association[:key].to_s}=".to_sym, nil
                        when :belongs_to
                          instance.send "#{association[:key].to_s}=".to_sym, nil
                        else
                          instance.send "#{association[:key].to_s}=".to_sym, nil
                      end

                      # puts "------- update future occurrence #{self.event.process_asoc(self, association).inspect} ------------"
                      instance_attributes["#{association[:key].to_s}_attributes".to_sym]=self.event.process_asoc(self, association)
                    end
                  end

                instance.update!(instance_attributes)
              end
            end
          end

          def update_event_schedule
            # puts "update_event_schedule called!"

            if self.event.single_instance
              self.event.start_at=self.start_at
              self.event.end_at=self.end_at
              return
            end
            if change_future_occurrences.nil? or !change_future_occurrences
              move_none_reoccuring_event
            else
              # puts "moving events!!!"
              # change_recurrence unless recurrence_changed?

              # split_reocurrence

              # # we are moving this and all following instances forward/backward
              move_events
            end
          end


        def move_event_back
            event = self.event.class.find(self.event.id)

            schedule = self.event.schedule

            # just change the schedule to start at the new start time
            schedule.start_time = self.start_at

            event.update_schedule(schedule)
          end

          def move_event_forward
            event = self.event.class.find(self.event.id)

            new_start_time = nil

            if event.recurrence == "d"
              # keep the original date but use the new time 
              new_start_time = get_start_of_event_day(self.event)
            else
              new_start_time = self.start_at
            end

            # abort "#{new_start_time.inspect} vs #{self.start_at.inspect}"

            if (event.recurrence.present? and event.recurrence != "d") or event.start_at.to_i < new_start_time.to_i
              # the first instance of the event will likely be missing since it was before the new_start_time

              # puts "-----------------HERE IN CONTENTIOUS IF!!-----------------"

              first_event_instance = self.event.original_event_id.present? ? nil : self.event.instances.order(start_at: :asc).first

              if first_event_instance.present? and first_event_instance.event_id == self.event_id
                # puts "-----------------CREATING NEW INSTANCE??? -----------------"
                new_event_attrs = first_event_instance.create_and_set_attributes(nil)
                new_event_attrs[:until_at] = self.event.start_at + self.event.duration
                new_event = self.event.class.create!(new_event_attrs)

                first_event_instance.update_columns(event_id: new_event.id)

                if event.recurrence != "d"
                  self.event.instances.where.not(id: self.id).where('end_at <= ?', self.start_at).update_all(event_id: new_event.id)
                end

                self.event.update_columns(original_event_id: new_event.id)
                self.event.class.where(original_event_id: self.event_id).update_all(original_event_id: new_event.id)
              end
            end

            # puts "-----------------AFTER CONTENTIOUS IF!!-----------------"
            # end this schedule at the next related event
            new_until_time = next_related_event.try(:start_at).present? ? next_related_event.try(:start_at) : self.until_at
            new_schedule = setup_schedule(self.event.recurrence, new_start_time, new_until_time, self.num_occurrences.to_i)

            # puts "-------- ABOUT TO DO THE STUFF!!!! ----------"
            new_schedule=avoid_repreating_new_start(new_schedule)

            # manually add occurrences for all the occurrences that occurred up until the old start_at time
            new_schedule=generate_reoccurance(new_schedule)

            # puts "new_until_time - #{new_until_time.class.inspect} #{new_until_time.inspect}"
            # puts "new_start_time - #{new_start_time.class.inspect} #{new_start_time.inspect}"
            # puts "#{new_schedule.inspect}"
            # abort new_schedule.inspect

            event.update_schedule(new_schedule)

            self.reload unless self.class.find_by(id: self.id).nil?
            puts "--------_SEEMS LIKE WE'RE DONE?????----------"
          end

        def update_related_events_schedule
            related_events.each do |related_event|
              # keep the original date but use the new time 
              new_start_time = new_time_based_on(related_event, :start_at)

              if related_event.instances.count == 0
                # no instances created yet so we can just happily change the event's schedule
                move_signleton(new_start_time, related_event)
              else
                # change the time on the earliest instance
                move_all_in_series(new_start_time, related_event)
              end
            end
          end

        def new_time_based_on(base, type)
            raise ArgumentError, "Invalid time type" if type != :start_at and type != :end_at

            if type == :start_at
              ret=get_start_of_event_day(base)
            elsif type == :end_at
              ret=get_end_of_event_day(base)
            end
            ret
          end

        private

        def end_recurrence_at_and_delete
          schedule = self.event.schedule

          schedule.recurrence_rules.first.until(self.start_at)

          event = self.event.class.find(self.event.id)

          event.schedule = schedule.to_yaml

          event.save
        end

        def move_none_reoccuring_event
          schedule = self.event.schedule

          schedule.add_exception_time self.saved_changes["start_at"].first.utc
          schedule.add_recurrence_time start_at.utc

          self.event.schedule = schedule.to_yaml

          self.event.save

          #removed due to causing bugs with moving without updating following
          #self.event.reload.update_start_end_times
        end

        def split_reocurrence
          if self.saved_changes["start_at"].first > self.event.start_at
            self.event.expand(start_at: self.event.start_at, end_at: self.saved_changes["start_at"].first)
          else
            self.event.expand(start_at: self.saved_changes["start_at"].first, end_at: self.event.start_at)
          end
        end


        def generate_reoccurance(new_schedule)
          self.event.schedule.occurrences(self.saved_changes["start_at"].first).each do |occurrence|

            # we don't want to add an occurrence for the old start time
            if occurrence.to_i != self.saved_changes["start_at"].first.to_i
              new_schedule.add_recurrence_time occurrence.to_time
            end
          end

          new_schedule
        end

        def avoid_repreating_new_start(new_schedule)
          # if the day changed but the time didn't we only want to exclude events in the period that was created
          # otherwise we want to exclude events from the beginning that are on the new time but are before the event instance
          # that was moved
          opening_time = self.event.start_at.to_i == new_schedule.start_time.to_i ? self.saved_changes["start_at"].first : self.event.start_at

          new_schedule.occurrences_between(opening_time, self.start_at).each do |occurrence|
            # the event is no longer happening in the period in which it was moved

            # we don't want to add an exception for the new start time
            if occurrence.to_i != self.start_at.to_i
              new_schedule.add_exception_time occurrence
            end
          end

          new_schedule
        end

        def get_start_of_event_day(obj)
          get__of_event_day(obj.start_at,self.start_at)
        end

        def get_end_of_event_day(obj)
          get__of_event_day(obj.end_at,self.end_at)
        end

        def get__of_event_day(obj, obj2)
          Time.new(obj.year, obj.month, obj.day, obj2.hour, obj2.min, obj2.sec, obj2.formatted_offset)
        end

        def move_signleton(new_start_time, related_event)
          schedule = setup_schedule(related_event.recurrence, new_start_time)

          related_event.schedule = schedule.to_yaml
          related_event.save
        end

        def move_all_in_series(new_start_time, related_event)
          first_instance = related_event.instances.order(start_at: :asc).first

          # keep the original date but use the new time
          new_end_time = new_time_based_on(first_instance, :end_at)

          first_instance.update(start_at: new_start_time, end_at: new_end_time,
                                change_future_occurrences: true, change_schedule: true)
        end

        def move_whole_schedual
          self.event.schedule.start_time = self.start_at
          self.event.save
          self.event.update_start_end_times
        end

        def move_events
          if is_first?
            # just change the schedule to start at the new start time
            move_whole_schedual
          else
            if self.saved_changes["start_at"].first > self.saved_changes["start_at"].last
              # the event instance has been moved back

              change_recurrence
              # move_event_back
            else
              # keep the original date but use the new time
              move_event_forward
            end

            # puts "--------- ABOUT TO UPDATE RELATED ------------"
            update_related_events_schedule
            # puts "--------- DONE WITH UPDATE RELATED ------------"
          end
        end
        def is_first?
          first_instance = self.event.instances.order(start_at: :asc).first

          first_instance.id == self.id
          #self.class.earlier_in_series(self.saved_changes["start_at"].first, self.event.id).count == 0
        end
      end
    end
  end
end