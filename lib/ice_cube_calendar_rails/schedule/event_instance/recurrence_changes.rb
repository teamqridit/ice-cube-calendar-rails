module IceCubeCalendarRails
  module Schedule

    module EventInstance

      module RecurrenceChanges

        def recurrence
          @recurrence ||= self.event.try(:recurrence)
        end

        def recurrence=(value)
          if recurrence != value
            @recurrence_changed = true 
          else
            @recurrence_changed = false
          end

          @recurrence = value
        end

        def recurrence_changed?
          @recurrence_changed == true ? true : false
        end

        def change_recurrence
          if self.event.single_instance
            event=self.event
            event.recurrence=self.recurrence
            event.until_at=self.until_at
            event.save!
          elsif !@recurrence.nil?
            is_first = self.class.earlier_in_series(self.start_at, self.event.id).count == 0

            if is_first
              move_all
            else
              move_some
            end
          else
            is_first = self.class.earlier_in_series(self.start_at, self.event.id).count == 0
            if is_first
              self.class.later_in_series(self.start_at, self.event.id).destroy_all
              event = self.event
              event.recurrence=nil
              event.single_instance=true
              event.save!
            else
              is_first = self.class.earlier_in_series(self.start_at, self.event.id).count == 0
              if is_first
                self.class.later_in_series(self.start_at, self.event.id).destroy_all
                event = self.event
                event.recurrence=nil
                event.single_instance=true
                event.save
              else
                old_event=self.event
                original_event_id = self.event.original_event_id.nil? ? self.event.id : self.event.original_event_id
                new_event_attributes = create_and_set_attributes(original_event_id)
                event = self.event.class.create!(new_event_attributes)
                self.update(event: event)
                event.recurrence=nil
                event.single_instance=true
                event.save
                h={}
                h[:until_at]= self.start_at-1
                if self.class.earlier_in_series(self.start_at,old_event.id).count==1
                  h[:recurrence]=nil
                  h[:single_instance]=true
                end
                old_event.update(h)
              end
            end
          end
        end

        def create_or_update_event_for_recurrence
          event = nil
          original_event_id = self.event.original_event_id.nil? ? self.event.id : self.event.original_event_id
          new_event_attributes = create_and_set_attributes(original_event_id)


          #TODO, wrap in a transaction to prevent race conditions
          updateable_event = self.event.class.where('start_at = ? AND original_event_id = ?', self.start_at, original_event_id).first
          if updateable_event.nil?
            event = self.event.class.create!(new_event_attributes)
          else
            event = updateable_event.update!(new_event_attributes)
          end

          event
        end

        def create_and_set_attributes(original_event_id)
          new_event_attributes={}

          new_event_attributes[:recurrence] = self.recurrence
          new_event_attributes[:start_at] = self.start_at
          new_event_attributes[:end_at] = self.end_at
          new_event_attributes[:original_event_id] = original_event_id

          event_attributes.each do |attribute|
            new_event_attributes[attribute] = self.send attribute
          end
          unless self.event.event_associations.nil?
            self.event.event_associations.each do |association|
              new_event_attributes["#{association[:key].to_s}_attributes".to_sym]=self.event.process_asoc(self, association)
            end
          end
          new_event_attributes
        end

        private
        def move_all
          # all we really need to do is change the recurrence rule of this event
          new_schedule = setup_schedule(self.recurrence, self.event.schedule.start_time)

          event = self.event.class.find(self.event.id)
          event.schedule = new_schedule.to_yaml

          event.save

          # now update the start and end times of all existing instances
          event.update_start_end_times
        end
        def move_some
          # first start by ending the instance's event just before it began
          expected_num_moved_occurences = nil

          if self.num_occurrences.present? and self.num_occurrences.to_i > 0          
            total_expected_occurrences = self.num_occurrences.to_i

            num_instances_before_current = self.event.instances.where('start_at < ?', self.start_at).count

            expected_num_moved_occurences = total_expected_occurrences - num_instances_before_current

            puts "-------- num_instances_before_current is #{num_instances_before_current.inspect} -----------"
            puts "-------- expected_num_moved_occurences is #{expected_num_moved_occurences.inspect} -----------"
          end

          end_old

          # now create a new event based on the current instance's attributes
          new_event = create_or_update_event_for_recurrence

          # find all the events that share this instance's original event id
          original_event_ids=find_related(new_event)

          # now that we have a new event, associate this and all future instances with it
          associate(new_event,original_event_ids)

          # delete no longer valid events, if any
          self.event.class.later(self.start_at, original_event_ids: original_event_ids).destroy_all

          event.update_start_end_times

          if expected_num_moved_occurences.present? and expected_num_moved_occurences > 0
            schedule = new_event.schedule

            schedule.rrules.each do |rule|
              rule.count(expected_num_moved_occurences)
            end

            new_event.schedule = schedule.to_yaml
            new_event.num_occurrences = expected_num_moved_occurences

            new_event.save

            new_event.instances.order(start_at: :asc).offset(expected_num_moved_occurences).destroy_all

            new_event.reload
            self.reload
          end

          new_event.update_start_end_times
        end
        
        def end_old
          new_schedule = setup_schedule(self.event.recurrence, self.event.schedule.start_time, self.start_at - 10.seconds)

          event = self.event.class.find(self.event.id)
          event.schedule = new_schedule.to_yaml

          event.save
        end
        def find_related(event)
          [event.original_event_id] + self.event.class.where(original_event_id: event.original_event_id).pluck(:id)
        end
        def associate(event,original)
          self.class.later_in_series(self.start_at, self.event.id,
                                     original_event_ids: original).update_all(event_id: event.id)
          self.update!(event: event)
        end
      end
    end
  end
end