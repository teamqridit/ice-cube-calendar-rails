module IceCubeCalendarRails
  module Schedule

    module EventInstance

      extend ActiveSupport::Concern
   
      # included do
      # end
   
      module ClassMethods
        include ::IceCubeCalendarRails::List::EventInstance::ClassMethods

        def ice_cube_event_instance(*args)

          options = {}

          options[:class_name] = (args.present? and args[0][:event_class].present?) ? args[0][:event_class] : self.name.gsub('Instance', '')

          cattr_accessor :event_attributes

          attr_accessor :change_future_occurrences, :change_schedule

          belongs_to :event, **options

          setup__callbacks

          include ::IceCubeCalendarRails::Schedule::EventInstance::LocalInstanceMethods

          attr_accessor :suppress_update_future_occurrences
        end

        private

        def setup__callbacks
          after_update :delete_instances, if: Proc.new { |i| i.saved_changes["deleted"].present? and i.saved_changes["deleted"].first == false and i.saved_changes["deleted"].last == true and i.this_and_all_following == true }

          after_update :change_recurrence, if: Proc.new { |i| i.recurrence_changed? }

          after_update :update_event_schedule, if: Proc.new { |i| i.saved_changes["start_at"].present? and i.saved_changes["start_at"].first != i.saved_changes["start_at"].last and change_schedule }

          after_update :update_future_occurrences, unless: Proc.new { |i| i.suppress_update_future_occurrences }

          before_save do
            self.duration = end_at.to_i - start_at.to_i
          end
        end

        private
      end

      module LocalInstanceMethods
        include ::IceCubeCalendarRails::List::EventInstance::InstanceMethods 
        include ::IceCubeCalendarRails::Schedule::EventInstance::Recurrence
        include ::IceCubeCalendarRails::Schedule::EventInstance::RecurrenceChanges
        include ::IceCubeCalendarRails::Schedule

      end
    end
  end
end

ActiveRecord::Base.send :include, IceCubeCalendarRails::Schedule::EventInstance