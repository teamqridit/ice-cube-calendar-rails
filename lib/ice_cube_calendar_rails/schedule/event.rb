module IceCubeCalendarRails
  module Schedule

    module Event

      extend ActiveSupport::Concern
   
      module ClassMethods
        include ::IceCubeCalendarRails::List::Event::ClassMethods

        RECURRENCE = {'Daily' => 'd', 'Weekly' => 'w', 'Bi-weekly' => '2w', 'Monthly' => 'm', 'Yearly' => 'y'}

        def ice_cube_event(*args)
          
          options = {}

          options[:class_name] = args[0][:instance_class].present? ? args[0][:instance_class] : self.name + 'Instance'
          options[:dependent] = :destroy
          options[:foreign_key] = 'event_id'
          options[:inverse_of] = :event

          make_accessors(args, options)

          make_relations(options)

          make_callbacks

          instance_klass.event_attributes = event_attributes

          include ::IceCubeCalendarRails::Schedule::Event::LocalInstanceMethods
        end

        def make_callbacks
          after_create :create_single_event_instance, unless: :is_recurring_event?

          validates :recurrence, inclusion: {in: RECURRENCE.values}, allow_nil: true

          before_save do
            set_schedule_end if until_at_changed? or num_occurrences_changed?

            remove_instances_after_until_at if until_at_changed? and until_at.present?

            remove_instances_after_last_occurrence if num_occurrences_changed? and num_occurrences < instances.count

            self.schedule = schedule.to_yaml if is_recurring_event?

            self.duration = end_at.to_i - start_at.to_i if end_at.present?

            self.single_instance = self.recurrence.blank?

            true
          end
        end

        def make_relations(options)
          has_many :instances, -> { extending ::IceCubeCalendarRails::List::EventInstance::Finders }, **options

          belongs_to :original_event, class_name: self.class.to_s
        end

        def make_accessors(args, options)
          attr_accessor :end_at

          cattr_accessor :event_attributes do
            args[0][:event_attributes]
          end

          cattr_accessor :event_associations do
            args[0][:event_associations]
          end

          cattr_accessor :instance_klass do
            options[:class_name].constantize
          end
        end


      end

      module LocalInstanceMethods
        include ::IceCubeCalendarRails::Schedule::Event::Recurrence
        include ::IceCubeCalendarRails::List::Event::InstanceMethods
        include ::IceCubeCalendarRails::Callbacks::Event
        include ::IceCubeCalendarRails::Schedule

        # def start_at
        #   @start_at.nil? ? schedule.start_time : @start_at
        # end

        def create_event_instance(start_at, end_at)
          # puts "------------ #{start_at.inspect} - #{end_at.inspect} -------------------------"

          start_at,end_at=to_utc_time(start_at,end_at)

          instance_klass_attributes = { start_at: start_at, end_at: end_at, event: self, original_start_at: start_at, original_end_at: end_at }
          instance_klass_attributes_for_find = instance_klass_attributes.dup

          previous_mirrored_instance = nil

          previous_mirrored_instance = find_previous_mirrored_instance(start_at) unless self.single_instance

          collect_attributes(instance_klass_attributes, previous_mirrored_instance)

          instance = instance_klass.find_or_initialize_by(instance_klass_attributes_for_find)

          unless instance.persisted?
            instance.assign_attributes(instance_klass_attributes)
            instance.save!
          end
        end


        def process_asoc(obj, asoc)
          case obj.class.reflect_on_association(asoc[:key]).macro
            when :has_many
              ret = []
              (obj.send asoc[:key]).each do |record|
                tmp = {}
                asoc[:values].each do |key|
                  tmp[key]=record.send key
                end
                ret << tmp
              end
              return ret
            when :has_one
              temp = (obj.send asoc[:key])
              tmp = {}
              asoc[:values].each do |key|
                tmp[key] = temp.send key
              end
              return tmp
            when :belongs_to
              temp = (obj.send asoc[:key])
              tmp = {}
              asoc[:values].each do |key|
                tmp[key] = temp.send key
              end
              return tmp
            else
              return []
          end
        end

        protected
        def is_recurring_event?
          !single_instance
        end

        private

        def to_utc_time(start_at, end_at)
          return start_at.to_time.utc, end_at.to_time.utc
        end

        def find_previous_mirrored_instance(start_at)
          instance_klass.where('event_id IN (?)', related_ids(self)).where(this_and_all_following: true)
              .where("start_at < ?", start_at).order(id: :desc).first
        end

        def collect_attributes(instance_klass_attributes, previous_mirrored_instance)
          event_attributes.each do |attribute|

            if previous_mirrored_instance.nil?
              instance_klass_attributes[attribute] = self.send attribute
            else
              instance_klass_attributes[attribute] = previous_mirrored_instance.send attribute
            end
          end

          instance_klass_attributes[:deleted] = previous_mirrored_instance.nil? ? false : previous_mirrored_instance.send(:deleted)

          unless event_associations.nil?
            event_associations.each do |association|
              instance_klass_attributes["#{association[:key].to_s}_attributes".to_sym]=process_asoc(self, association)
            end
          end
        end
      end
    end
  end
end

ActiveRecord::Base.send :include, IceCubeCalendarRails::Schedule::Event