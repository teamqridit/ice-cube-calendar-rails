module IceCubeCalendarRails
  module Schedule

    module Event

      module Recurrence

        def schedule
          @schedule = read_attribute(:schedule) if @schedule.nil?

          if @schedule.nil?
            @schedule = setup_schedule(self.recurrence, self.start_at, self.until_at, self.num_occurrences)
          else
            @schedule = IceCube::Schedule.from_yaml(@schedule) unless @schedule.is_a? IceCube::Schedule
          end
          @schedule
        end

        def update_schedule(new_schedule)
          self.schedule = new_schedule.to_yaml
          self.save!
          self.update_start_end_times
        end
      end
    end
  end
end