module IceCubeCalendarRails
  module Schedule

    def setup_schedule(recurrence_rule, start, until_date = nil, num_occurrences = nil)
      num_occurrences = nil if num_occurrences == 0

      new_schedule = IceCube::Schedule.new(start.in_time_zone('UTC'))

      if until_date.nil? and num_occurrences.nil?
        rule(new_schedule, recurrence_rule)
      else
        if until_date.present?
          rule_until(new_schedule, recurrence_rule, until_date)
        else
          rule_with_count(new_schedule, recurrence_rule, num_occurrences)
        end
      end
    end

    def related_ids(event)
      if event.original_event_id.nil?
        original_event_ids = [event.id]
        original_event_ids = original_event_ids + event.class.where(original_event_id: event.id).pluck(:id)
      else
        original_event_ids = [event.original_event_id] + event.class.where.not(id: event.id).where(original_event_id: event.original_event_id).pluck(:id)
      end
    end

    private
      def rule(new_schedule, type)
        case type
          when 'd'  then new_schedule.rrule(IceCube::Rule.daily)
          when 'w' then  new_schedule.rrule(IceCube::Rule.weekly)
          when '2w' then  new_schedule.rrule(IceCube::Rule.weekly(2))
          when 'm'  then  new_schedule.rrule(IceCube::Rule.monthly)
          when 'y'  then  new_schedule.rrule(IceCube::Rule.yearly)
        end
        new_schedule
      end

      def rule_until(new_schedule, type, until_date)
        case type
          when 'd'  then new_schedule.rrule(IceCube::Rule.daily.until(until_date))
          when 'w' then  new_schedule.rrule(IceCube::Rule.weekly.until(until_date))
          when '2w' then  new_schedule.rrule(IceCube::Rule.weekly(2).until(until_date))
          when 'm'  then  new_schedule.rrule(IceCube::Rule.monthly.until(until_date))
          when 'y'  then  new_schedule.rrule(IceCube::Rule.yearly.until(until_date))
        end
        new_schedule
      end

      def rule_with_count(new_schedule, type, num_occurrences)
        case type
          when 'd'  then new_schedule.rrule(IceCube::Rule.daily.count(num_occurrences))
          when 'w' then  new_schedule.rrule(IceCube::Rule.weekly.count(num_occurrences))
          when '2w' then  new_schedule.rrule(IceCube::Rule.weekly(2).count(num_occurrences))
          when 'm'  then  new_schedule.rrule(IceCube::Rule.monthly.count(num_occurrences))
          when 'y'  then  new_schedule.rrule(IceCube::Rule.yearly.count(num_occurrences))
        end
        new_schedule
      end
  end
end