class CreateEvents < ActiveRecord::Migration[4.2]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :start_at
      t.text :schedule
      t.string :recurrence
      t.integer :duration
      t.datetime :until_at
      t.integer :num_occurrences
      t.integer :original_event_id, index: true
      t.boolean :single_instance, default: true

      t.timestamps null: false
    end
  end
end
