class CreateEventInstances < ActiveRecord::Migration[4.2]
  def change
    create_table :event_instances do |t|
      t.string :name
      t.text :description
      t.datetime :start_at
      t.datetime :end_at
      t.datetime :original_start_at
      t.datetime :original_end_at
      t.integer :duration
      t.integer :instance_position
      t.boolean :this_and_all_following, default: false
      t.boolean :deleted, default: false
      t.references :event, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
