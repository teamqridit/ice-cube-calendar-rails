# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2015_06_03_123803) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "event_instances", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "original_start_at"
    t.datetime "original_end_at"
    t.integer "duration"
    t.integer "instance_position"
    t.boolean "this_and_all_following", default: false
    t.boolean "deleted", default: false
    t.integer "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_instances_on_event_id"
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "start_at"
    t.text "schedule"
    t.string "recurrence"
    t.integer "duration"
    t.datetime "until_at"
    t.integer "num_occurrences"
    t.integer "original_event_id"
    t.boolean "single_instance", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["original_event_id"], name: "index_events_on_original_event_id"
  end

  add_foreign_key "event_instances", "events"
end
