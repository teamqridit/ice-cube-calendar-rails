class Event < ActiveRecord::Base
  ice_cube_event instance_class: 'EventInstance', event_attributes: [:name, :description]
end
