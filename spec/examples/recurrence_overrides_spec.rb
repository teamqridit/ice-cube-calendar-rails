require 'rails_helper'

RSpec.describe "IceCubeCalendarRails::Schedule::EventInstance::Recurrence" do

  describe "changing the start and end date of an event instance" do
    before do
      @event = create(:daily_event)
    end

    context "to a start and end date on the same date as another occurrence of the same event" do

      it "remains aware of the event even when it no longer fits in with the regular recurrence pattern" do
        # expand up to tomorrow's event
        instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 1.day).last

        # the event now starts the day after tomorrow but an hour before that day's regular event
        instance.update(start_at: @event.start_at + 2.days - 1.hour, end_at: @event.end_at + 2.days - 1.hour, change_schedule: true)

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.day)

        # we should have 1 occurrence since tomorrow's event instance was moved
        expect(event_instances.count).to eq 1

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 7.days)          

        # we should have 8 occurrences between now and one week from now
        expect(event_instances.count).to eq 8
      end
    end

    context "changing the current and all future recurring instances event period by a specific amount of time" do

      context "affected instances do not exist" do
        it "properly handle changes to the very first instance" do
          instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days).first

          instance.update(start_at: @event.start_at - 2.days, end_at: @event.end_at - 2.days, 
            change_future_occurrences: true, change_schedule: true)   

          event_instances = Event.all_instances(start_at: @event.start_at - 2.days, end_at: @event.end_at + 7.days)          

          # we should have 10 occurrences between two days ago and one week from now
          expect(event_instances.count).to eq 10

        end

        it "properly handles changes to any arbitrary instance" do
          instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days)[2]

          instance.update(start_at: @event.start_at + 3.days, end_at: @event.end_at + 3.days, 
            change_future_occurrences: true, change_schedule: true)             

          event_instances = Event.all_instances(start_at: @event.start_at - 1.minute, end_at: @event.end_at + 7.days)          

          # there should no longer be an instance starting 2 days from now
          expect(event_instances.where(start_at: @event.start_at + 2.days).count).to eq 0

          # puts Event.first.schedule.inspect
          # event_instances.each do |instance|
          #   puts "#{instance.start_at.inspect} - #{instance.end_at.inspect}"
          # end

          # we should have 7 occurrences between now and one week from now because we are now skipping a day
          expect(event_instances.count).to eq 7
        end
      end

      context "affected instances exist" do
        it "properly handle changes to the very first instance" do
          instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days).first

          instance.update(start_at: @event.start_at - 2.days, end_at: @event.end_at - 2.days, 
            change_future_occurrences: true, change_schedule: true)   

          event_instances = Event.all_instances(start_at: @event.start_at - 2.days, end_at: @event.end_at + 7.days)          

          # we should have 10 occurrences between two days ago and one week from now
          expect(event_instances.count).to eq 10

        end

        it "properly handles changes to any arbitrary instance" do
          instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days)[2]

          instance.update(start_at: @event.start_at + 3.days, end_at: @event.end_at + 3.days, 
            change_future_occurrences: true, change_schedule: true)   

          event_instances = Event.all_instances(start_at: @event.start_at - 1.second, end_at: @event.end_at + 7.days)

          # there should no longer be an instance starting 2 days from now
          expect(event_instances.where(start_at: @event.start_at + 2.days).count).to eq 0

          # we should have 7 occurrences between now and one week from now because we are now skipping a day
          expect(event_instances.count).to eq 7

          # event_instances.each do |instance|
          #   puts instance.inspect
          # end
        end
      end
    end
  end

  describe "changing any event attribute" do
    before do
      @event = create(:daily_event)
    end

    context "a single instance" do
      it "changes the attributes of a single instance of an event" do
        # trivial test to be honest...

        instance = @event.expand(start_at: @event.start_at + 2.days, end_at: @event.end_at + 2.days).first

        instance.update(name: "New name for event instance", description: "new description")

        # just make sure any future occurrences aren't affected

        future_instance = @event.expand(start_at: @event.start_at + 3.days, end_at: @event.end_at + 3.days).first

        expect(future_instance.name).to eq @event.name
        expect(future_instance.description).to eq @event.description
      end
    end

    context "current and future instances" do
      it "changes the attributes of the instance and that of any expanded future instances" do
        instance = @event.expand(start_at: @event.start_at + 2.days, end_at: @event.end_at + 7.days).first

        instance.update(name: "New name for event instance", description: "new description", this_and_all_following: true)

        # now make sure the future occurrences have had their attributes changed

        future_instances = EventInstance.where("start_at >= ? AND end_at <= ?", @event.start_at + 3.days, @event.end_at + 7.days)

        instance.reload

        future_instances.each do |future_instance|

          expect(future_instance.name).to eq instance.name
          expect(future_instance.description).to eq instance.description
        end
      end

      it "changes the attributes of the instance and that of any unexpanded future instances" do
        instance = @event.expand(start_at: @event.start_at + 2.days, end_at: @event.end_at + 2.days).first

        instance.update(name: "New name for event instance", description: "new description", this_and_all_following: true)

        future_instances = @event.expand(start_at: @event.start_at + 3.days, end_at: @event.end_at + 7.days)

        future_instances.each do |future_instance|
          expect(future_instance.name).to eq instance.name
          expect(future_instance.description).to eq instance.description
        end
      end

      context "expanded future instances" do
        it "uses the attributes from the closest changed instance that applied the 'current and future instances' rule" do
          first_instance = @event.expand(start_at: @event.start_at + 1.days, end_at: @event.end_at + 7.days).first

          first_instance.update(name: "New name for event first overrided instance", description: "new description for first", this_and_all_following: true)

          second_instance = EventInstance.find_by_date_range(@event.start_at + 2.days, @event.end_at + 2.days).first

          second_instance.update(name: "New name for second overrided event instance", description: "new description for second", this_and_all_following: true)

          expanded_future_instances = EventInstance.where("start_at >= ? AND end_at <= ?", @event.start_at + 3.days, @event.end_at + 7.days)

          second_instance.reload

          expanded_future_instances.each do |future_instance|
            expect(future_instance.name).to eq second_instance.name
            expect(future_instance.description).to eq second_instance.description
          end

        end
      end

      context "unexpanded future instances" do
        it "uses the attributes from the closest changed instance that applied the 'current and future instances' rule" do
          first_instance = @event.expand(start_at: @event.start_at + 1.days, end_at: @event.end_at + 1.days).first

          first_instance.update(name: "New name for event first overrided instance", description: "new description for first", this_and_all_following: true)

          second_instance = @event.expand(start_at: @event.start_at + 2.days, end_at: @event.end_at + 2.days).first

          second_instance.update(name: "New name for second overrided event instance", description: "new description for second", this_and_all_following: true)

          unexpanded_future_instances = @event.expand(start_at: @event.start_at + 3.days, end_at: @event.end_at + 7.days)

          second_instance.reload

          unexpanded_future_instances.each do |future_instance|
            expect(future_instance.name).to eq second_instance.name
            expect(future_instance.description).to eq second_instance.description
          end

        end
      end
    end
  end

  describe "changing when a recurring series ends" do

    context "no until date initially" do
      before do
        @event = create(:daily_event)
      end

      context "changing the end date for a series to some point after the last expanded event" do

        it "does not create any event instances past the end point" do
          
          @event.expand(start_at: @event.start_at + 1.day, end_at: @event.end_at + 3.days)

          @event.update(until_at: @event.end_at + 4.days)

          event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.month)        

          # we should have 5 event instanes, today plus 4 for the next for days
          expect(event_instances.count).to eq 5
        end
      end

      context "changing the end date for a series to some point before the last expanded event" do
        it "deletes and does not create any instances past the end point" do

          @event.expand(start_at: @event.start_at + 1.day, end_at: @event.end_at + 7.days)

          @event.update(until_at: @event.end_at + 4.days)

          event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.month)        

          # we should have 5 event instanes, today plus 4 for the next for days
          expect(event_instances.count).to eq 5
        end
      end 
    end

    context "an inital until date" do
      before do
        @event = create(:daily_event, until_at: (Time.now + 1.hour).utc + 3.days)
      end

      it "creates instances accordingly when a new until date is set" do
        @event.expand(start_at: @event.start_at + 1.day, end_at: @event.end_at + 7.days)

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.month)        

        expect(event_instances.count).to eq 4

        @event.update(until_at: @event.end_at + 7.days)

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.month)        

        expect(event_instances.count).to eq 8
      end

      it "creates instances accordingly when the until date is removed" do
        @event.expand(start_at: @event.start_at + 1.day, end_at: @event.end_at + 7.days)

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.month)        

        expect(event_instances.count).to eq 4

        @event.update(until_at: nil)

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 2.weeks)        

        expect(event_instances.count).to eq 15
      end
    end   
  end

  describe "deleting an event" do
    before do
      @event = create(:daily_event)
    end

    context "a single instance" do
      it "deletes a single instance of an event" do
        # trivial test to be honest...

        instance = @event.expand(start_at: @event.start_at + 2.days, end_at: @event.end_at + 2.days).first

        instance.update(deleted: true)

        # just make sure any future occurrences aren't affected

        future_instance = @event.expand(start_at: @event.start_at + 3.days, end_at: @event.end_at + 3.days).first

        expect(future_instance.deleted).to eq false
      end
    end

    context "current and future instances" do

      it "just deletes the entire event if the deleted instance is the very first instance" do
        instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at).first

        instance.update(deleted: true, this_and_all_following: true)

        expect(Event.count).to eq 0
        expect(EventInstance.count).to eq 0
      end

      it "stops the schedule at the deleted instance and deletes any future instances" do

        instance = @event.expand(start_at: @event.start_at + 2.days, end_at: @event.end_at + 7.days).first

        instance.update(deleted: true, this_and_all_following: true)

        future_instances = EventInstance.where("start_at >= ? AND end_at <= ?", @event.start_at + 3.days, @event.end_at + 7.days)

        expect(future_instances.count).to eq 0
      end
    end
  end
end
