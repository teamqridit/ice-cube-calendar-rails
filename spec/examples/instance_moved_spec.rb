require 'rails_helper'

RSpec.describe "moving events" do
  context "single instance" do
    before(:each) do
      @event = create(:single_instance_event,start_at: Time.now+5.days, end_at: Time.now+5.days+1.hours)
    end
    it "can be moved before the start_at" do
      instance=@event.instances.first

      instance.update!({start_at:instance.start_at-1.days,end_at:instance.end_at-1.days, change_schedule:true})

      expect(instance.class.exists?(instance.id)).to eq true
    end
    it "can be moved to a specified time" do
      instance=@event.instances.first
      h={start_at:instance.start_at+1.day,end_at:instance.end_at+1.day, change_schedule:true,this_and_all_following: false}
      instance.update(h)
      instance.reload
      expect(instance.start_at.to_i).to eq h[:start_at].to_i
    end
  end
  context "recurring" do
    before(:each) do
      @event = create(:daily_event)
    end

    it "can be move instance to before start_at of the event" do
      instance=@event.expand(start_at:@event.start_at,end_at: @event.start_at+10.days).first
      instance.update!({start_at:@event.start_at-1.days,end_at:@event.start_at-1.days+1.hours, change_schedule:true,this_and_all_following: false})
      expect(instance.class.exists?(instance.id)).to eq true
    end

    it "can move an instance to the same time as another instance" do
      instances=@event.expand(start_at:@event.start_at,end_at: @event.start_at+2.days)
      first=instances.first
      second=instances.second
      second.update!({start_at:second.start_at-1.days,end_at:second.end_at-1.days, change_schedule:true,this_and_all_following: false})
      expect(first.class.exists?(first.id)).to eq true
      expect(first.class.exists?(second.id)).to eq true
      expect(first.start_at.to_i).to eq second.start_at.to_i
    end

    it "can be moved to a specified time" do
      @event.expand(start_at:@event.start_at,end_at: @event.start_at+2.days)
      instance=@event.instances.first
      h={start_at:instance.start_at+1.day,end_at:instance.end_at+1.day, change_schedule:true,this_and_all_following: false}
      instance.update(h)
      instance.reload
      expect(instance.start_at.to_i).to eq h[:start_at].to_i
    end

    it "does not add extras" do
      instances=@event.expand(start_at:@event.start_at,end_at: @event.start_at+2.days)
      expect(@event.instances.count).to eq 3
      first=instances.first
      second=instances.second
      second_s=second.start_at
      third=instances.third
      third_s=third.start_at
      h={start_at: first.start_at+1.day+1.hour,end_at:first.end_at+1.day+1.hour,change_schedule: true,this_and_all_following: false}
      first.update!(h)
      @event.reload
      first.reload
      second.reload
      third.reload
      expect(@event.instances.count).to eq 3
      expect(first.start_at.to_i).to eq  h[:start_at].to_i
      expect(second.start_at.to_i).to eq second_s.to_i
      expect(third.start_at.to_i).to eq third_s.to_i
    end
  end
end