require 'rails_helper'

RSpec.describe "IceCubeCalendarRails::Schedule::EventInstance::RecurrenceChanges" do
  
  describe "changing the recurrence rule" do
    before do
      @event = create(:daily_event)
    end

    context "very first instance in the event object's series" do

      it "simply changes the recurrence rule" do
        # expand tomorrow's event
        instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at).first

        # the event is now weekly
        instance.update(recurrence: 'w')

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 7.day)

        # we should have 2 occurrence since today's event instance is now weekly
        expect(event_instances.count).to eq 2

        # just making sure that no new events are created
        expect(Event.count).to eq 1
      end

      it "changes the occurrence period for any already expanded events" do
        # expand the next 7 days' event
        instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days).first

        # the event is now weekly
        instance.update(recurrence: 'w')

        event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 7.day)

        # we should have 2 occurrence since today's event instance is now weekly
        expect(event_instances.count).to eq 2

        # just making sure that no new events are created
        expect(Event.count).to eq 1
      end
    end

    context "an instance at some arbitrary position in the event series" do

      context "unexpanded events" do
        it "creates a new event linked to the original" do
          # expand today and tomorrow's event
          tomorrow_instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 1.day).last

          # the event is now weekly
          tomorrow_instance.update(recurrence: 'w')

          event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 8.day)

          # we should have 3 occurrence since tomorrow's event instance is now weekly
          expect(event_instances.count).to eq 3

          # we now have two seperate but somehow related events
          expect(Event.count).to eq 2
        end
      end

      context "expanded events" do
        it "creates a new event linked to the original" do
          # expand today and tomorrow's event
          tomorrow_instance = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.day)[1]

          # the event is now weekly
          tomorrow_instance.update(recurrence: 'w')

          event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 8.day)

          # we should have 3 occurrence since tomorrow's event instance is now weekly
          expect(event_instances.count).to eq 3

          # we now have two seperate but somehow related events
          expect(Event.count).to eq 2
        end

        it "deletes events that are no longer valid and updates ones that are" do
          # expand the next 7 days' event
          instances = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 12.days)

          tomorrow_instance = instances[1]

          # the event that is currently 3 days from now
          future_instance_1 = instances[3]

          # our event, after 3 daily occurrences is now monthly
          future_instance_1.update(recurrence: 'm') 

          # the event that is currently 5 days from now
          future_instance_2 = instances[5]

          # our event, after 3 daily occurrences and 2 monthly occurrences is now bi weekly
          future_instance_2.reload.update(recurrence: '2w')

          expect(Event.count).to eq 3

          # screw this, tomorrow's event is now weekly
          tomorrow_instance.reload.update(recurrence: 'w')

          event_instances = Event.all_instances(start_at: @event.start_at + 1.day, end_at: @event.end_at + 1.day + 4.weeks)

          # we should have 5 occurrence since tomorrow's event instance is now weekly
          expect(event_instances.count).to eq 5

          # we are now back to two seperate but somehow related events
          expect(Event.count).to eq 2
        end


      end

      describe "carrying forward changes across related events" do

        it "updates event attributes changed with the 'this-and-all-following' operative across different related instances" do
          # expand the next 7 days' event
          instances = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days)

          tomorrow_instance = instances[1]

          # the event that is currently 3 days from now
          future_instance_1 = instances[3]

          # our event, after 3 daily occurrences is now weekly
          future_instance_1.update(recurrence: 'w') 

          # the event that is currently 5 days from now
          future_instance_2 = instances[5]

          # our event, after 3 daily occurrences and 2 weekly occurrences is now bi weekly
          future_instance_2.reload.update(recurrence: '2w')

          tomorrow_instance.update(name: "New name for event instance", description: "new description", this_and_all_following: true)

          event_instances = Event.all_instances(start_at: @event.start_at + 1.day, end_at: @event.end_at + 1.month)

          event_instances.each do |instance|

            expect(instance.name).to eq tomorrow_instance.name
            expect(instance.description).to eq tomorrow_instance.description
          end
        end

        it "properly changes the start and end dates for instances updated with the 'this-and-all-following' operative" do
          # expand the next 7 days' event
          instances = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days)

          tomorrow_instance = instances[1]

          # the event that is currently 3 days from now
          future_instance_1 = instances[3]

          # our event, after 3 daily occurrences is now weekly
          future_instance_1.reload.update(recurrence: 'w') 

          # the event that is currently 5 days from now
          future_instance_2 = instances[5]

          # our event, after 3 daily occurrences and 2 weekly occurrences is now bi weekly
          future_instance_2.reload.update(recurrence: '2w')

          new_start_time = @event.start_at + 3.days + 2.hours
          
          new_end_time = @event.end_at + 3.days + 2.hours

          tomorrow_instance.reload.update(start_at: new_start_time, end_at: new_end_time, 
            change_future_occurrences: true, change_schedule: true)   

          # puts tomorrow_instance.inspect

          event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.month)

          # there should no longer be an instance starting tomorrow
          expect(event_instances.where(start_at: @event.start_at + 1.days).count).to eq 0
          # puts Event.all[1].inspect

          # event_instances.order(id: :asc).each do |instance|
          #   puts instance.inspect
          # end

          event_instances.order(start_at: :asc).each_with_index do |instance, i|

            if (i > 0)
              expect(instance.start_at.hour).to eq new_start_time.hour
              expect(instance.start_at.min).to eq new_start_time.min
              expect(instance.start_at.sec).to eq new_start_time.sec

              expect(instance.end_at.hour).to eq new_end_time.hour
              expect(instance.end_at.min).to eq new_end_time.min
              expect(instance.end_at.sec).to eq new_end_time.sec
            else
              expect(instance.start_at.hour).to eq @event.start_at.hour
              expect(instance.start_at.min).to eq @event.start_at.min
              expect(instance.start_at.sec).to eq @event.start_at.sec

              expect(instance.end_at.hour).to eq @event.end_at.hour
              expect(instance.end_at.min).to eq @event.end_at.min
              expect(instance.end_at.sec).to eq @event.end_at.sec
            end
            
          end
        end

        it "properly deletes instances deleted with the 'this-and-all-following' operative" do
          # expand the next 7 days' event
          instances = @event.expand(start_at: @event.start_at, end_at: @event.end_at + 7.days)

          tomorrow_instance = instances[1]

          # the event that is currently 3 days from now
          future_instance_1 = instances[3]

          # our event, after 3 daily occurrences is now weekly
          future_instance_1.reload.update(recurrence: 'w') 

          # the event that is currently 5 days from now
          future_instance_2 = instances[5]

          # our event, after 3 daily occurrences and 2 weekly occurrences is now bi weekly
          future_instance_2.reload.update(recurrence: '2w')

          new_start_time = @event.start_at + 3.days + 2.hours
          
          new_end_time = @event.end_at + 3.days + 2.hours

          tomorrow_instance.reload.update(deleted: true, this_and_all_following: true)

          event_instances = Event.all_instances(start_at: @event.start_at, end_at: @event.end_at + 1.year)

          # puts Event.first.schedule.to_yaml

          # event_instances.each do |instance|
          #   puts instance.inspect
          # end

          # there should only be one instance left, i.e. today's
          expect(event_instances.where(deleted: false).count).to eq 1
        end

      end

    end
  end
  describe "setting the re-occurence rule on a non reocurring event instance" do
    before(:each) do
      @event = create(:single_instance_event,start_at: Time.now, end_at: Time.now+1.hours)
    end
    it "properly sets the re-ocurrence" do
      instance=@event.instances.first
      instance.update!(recurrence:'d',this_and_all_following:false,until_at: nil)
      @event.reload
      expect(@event.recurrence).to eq 'd'
    end
  end
  describe "removing the reoccurence from a repeating event" do
    before(:each) do
      @event = create(:daily_event)
    end
    it "properly removed the re-ocurrence when it is the first" do
      instance=@event.expand(start_at: @event.start_at,end_at: @event.start_at+7.days).first
      instance.update!(recurrence:nil,this_and_all_following:true,until_at: nil)
      instance.reload
      @event.reload
      expect(instance.event).to eq @event
      expect(@event.recurrence).to be_nil
      expect(@event.single_instance).to be true
      expect(@event.instances.count).to be 1
    end
    it "properly splits and changes the re-ocurrence when it is second" do
      instances=@event.expand(start_at: @event.start_at,end_at: @event.start_at+7.days)
      first=instances.first
      second=instances.second
      second.update!(recurrence:nil,this_and_all_following:true,until_at: nil)
      first.reload
      second.reload
      expect(first.event_id).to eq @event.id
      expect(first.event_id).to_not eq second.event_id
      expect(second.event_id).to_not eq @event.id
      expect(first.event.until_at.to_i).to be < second.start_at.to_i
      expect(second.event.recurrence).to be_nil
      expect(first.event.recurrence).to be_nil
      expect(first.event.single_instance).to be true
      expect(first.event.instances.count).to be 1
      expect(second.event.instances.count).to be 1
    end
    it "properly splits and changes the re-ocurrence when it is not first or second" do
      instances=@event.expand(start_at: @event.start_at,end_at: @event.start_at+7.days)
      first=instances.first
      third=instances.third
      third.update!(recurrence:nil,this_and_all_following:true,until_at: nil)
      first.reload
      third.reload
      expect(first.event_id).to eq @event.id
      expect(first.event_id).to_not eq third.event_id
      expect(third.event_id).to_not eq @event.id
      expect(first.event.until_at.to_i).to be < third.start_at.to_i
      expect(third.event.recurrence).to be_nil
      expect(first.event.recurrence).to eq 'd'
      expect(first.event.single_instance).to be false
      expect(first.event.instances.count).to be 2
      expect(third.event.instances.count).to be 1
    end
  end
end