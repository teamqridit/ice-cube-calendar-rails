require 'rails_helper'

RSpec.describe "IceCubeCalendarRails::List" do
  
  describe "single instance events" do

    before do
      @events = []
      10.times.each do |i|
        start_at = Time.now + i.days
        end_at = start_at + 2.hours

        @events << create(:single_instance_event, start_at: start_at, end_at: end_at)
      end
    end

    context "all Events" do

      it "returns all instances within a date range that encompasses all the events" do
        event_instances = Event.all_instances(start_at: Time.now - 1.hour, end_at: Time.now + 10.days)

        expect(event_instances.count).to eq 10

        # check that the attributes defined on the instance are the same as those defined on the event itself by default
        expect(event_instances.first.name).to eq @events.first.name
        expect(event_instances.first.description).to eq @events.first.description

        expect(event_instances.last.name).to eq @events.last.name
        expect(event_instances.last.description).to eq @events.last.description
      end

      it "returns all instances within a given date range that ends before the last event ends" do
        event_instances = Event.all_instances(start_at: Time.now - 1.hour, end_at: Time.now + 5.days + 2.hours)

        expect(event_instances.count).to eq 6

        # check that the attributes defined on the instance are the same as those defined on the event itself by default
        expect(event_instances.first.name).to eq @events.first.name
        expect(event_instances.first.description).to eq @events.first.description
      end

      it "throws an error if the start date is not specified" do
        expect{Event.all_instances(end_date: Time.now)}.to raise_error(ArgumentError)
      end

      it "throws an error if the end date is not specified" do
        expect{Event.all_instances(start_at: Time.now)}.to raise_error(ArgumentError)
      end
    end
    
    context "specific Event" do
      it "returns a single instance of an event" do
        event = Event.first

        event_instance = event.instances.first

        # check that the attributes defined on the instance are the same as those defined on the event itself by default
        expect(event_instance.name).to eq @events.first.name
        expect(event_instance.description).to eq @events.first.description
      end
    end
  end

  describe "recurring events" do
    before do
      @events = []

      @start_at = Time.now
      @end_at = @start_at + 2.hours

      @events << create(:daily_event, start_at: @start_at, end_at: @end_at)
      @events << create(:weekly_event, start_at: @start_at, end_at: @end_at)
      @events << create(:biweekly_event, start_at: @start_at, end_at: @end_at)
      @events << create(:monthly_event, start_at: @start_at, end_at: @end_at)
      @events << create(:yearly_event, start_at: @start_at, end_at: @end_at)
    end

    context "all Events" do

      it "returns all instances within a given duration" do
        # first start with a range that only encompasses a very small range
        event_instances = Event.all_instances(start_at: @start_at - 1.hour, end_at: @end_at + 1.day)

        # 2 occurrences of the daily event and one of all the others
        expect(event_instances.count).to eq 6        

        # now try to wrangle recurring events from a larger list
        event_instances = Event.all_instances(start_at: @start_at - 1.hour, end_at: @end_at + 7.days)

        # 8 occurrences of the daily event, two occurrences of the weekly event, one of all the others
        expect(event_instances.count).to eq 13 

        # now try to get all the events from a date range that starts later than the start date
        event_instances = Event.all_instances(start_at: (@start_at + 3.days) - 1.hour, end_at: @end_at + 7.day)

        # 5 occurrences of the daily event and 1 occurrence of a weekly event and none of any other kind
        expect(event_instances.count).to eq 6       

        # check that the attributes defined on the instance are the same as those defined on the event itself by default
        event_instances.each do |instance|
          expect(instance.name).to eq instance.event.name
          expect(instance.description).to eq instance.event.description
        end
      end

      it "returns all instances within a year" do
        event_instances = Event.all_instances(start_at: @start_at - 1.hour, end_at: @end_at + 1.year)

        # event_instances.each do |instance|
        #   puts instance.inspect
        # end

        num_daily_occurrences = ((@start_at.year % 4 == 0) or ((@end_at + 1.year).year % 4 == 0)) ? 366 : 365

        expect(event_instances.count).to eq num_daily_occurrences + 53 + 27 + 13 + 2

        # check that the attributes defined on the instance are the same as those defined on the event itself by default
        event_instances.each do |instance|
          expect(instance.name).to eq instance.event.name
          expect(instance.description).to eq instance.event.description
        end
      end

    end


  end

end
