require 'rails_helper'

RSpec.describe "obscure" do
  before(:each) do
    @event = create(:single_instance_event,start_at: Time.now+5.days, end_at: Time.now+1.weeks+5.days+1.hours,single_instance:nil)
  end

  it "can be longer then a day" do
    expect(@event.duration).to be > 24.hours.to_i
  end
  it "sets the single instance field" do
    expect(@event.single_instance).to be_truthy
  end
end