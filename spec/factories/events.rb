FactoryGirl.define do

  factory :single_instance_event, class: Event do
    name { "Test Event - No recurrence - #{SecureRandom.uuid}" }
    description "A description of this event and so on and so forth"
  end

  factory :daily_event, class: Event do
    name "Test Event - Daily"
    description "A description of this event and so on and so forth"
    single_instance false
    start_at Time.now.utc
    end_at (Time.now + 1.hour).utc
    recurrence 'd'
  end

  factory :weekly_event, class: Event do
    name "Test Event - Weekly"
    description "A description of this event and so on and so forth"
    single_instance false
    start_at Time.now.utc
    end_at (Time.now + 1.hour).utc
    recurrence 'w'
  end

  factory :biweekly_event, class: Event do
    name "Test Event - Biweekly"
    description "A description of this event and so on and so forth"
    single_instance false
    start_at Time.now.utc
    end_at (Time.now + 1.hour).utc
    recurrence '2w'
  end

  factory :monthly_event, class: Event do
    name "Test Event - Monthly"
    description "A description of this event and so on and so forth"
    single_instance false
    start_at Time.now.utc
    end_at (Time.now + 1.hour).utc
    recurrence 'm'
  end

  factory :yearly_event, class: Event do
    name "Test Event - Yearly"
    description "A description of this event and so on and so forth"
    single_instance false
    start_at Time.now.utc
    end_at (Time.now + 1.hour).utc
    recurrence 'y'
  end

end
