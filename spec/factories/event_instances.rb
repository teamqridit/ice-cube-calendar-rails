FactoryGirl.define do
  factory :event_instance do
    name "MyString"
    description "MyText"
    schedule "MyText"
    start_date "2015-06-03 14:38:03"
    end_date "2015-06-03 14:38:03"
    instance_position 1
    event nil
  end

end
